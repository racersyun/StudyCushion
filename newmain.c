/*
 * File:   newmain.c
 * Author: racer
 *
 * Created on 2016? 10? 11? (?), ?? 1:13
 */

#include <C:\Program Files (x86)\PICC\Devices\16F883.h>
#device pic16f883 *=8 adc=10
#use delay(clock=4000000)
#use rs232(baud=9600,xmit=PIN_C6,rcv=PIN_C7)

// CONFIG1
//#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator: High-speed crystal/resonator on RA6/OSC2/CLKOUT and RA7/OSC1/CLKIN)
//#pragma config WDTE = OFF        // Watchdog Timer Enable bit (WDT enabled)
//#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
//#pragma config MCLRE = ON       // RE3/MCLR pin function select bit (RE3/MCLR pin function is MCLR)
//#pragma config CP = OFF         // Code Protection bit (Program memory code protection is disabled)
//#pragma config CPD = OFF        // Data Code Protection bit (Data memory code protection is disabled)
//#pragma config BOREN = ON       // Brown Out Reset Selection bits (BOR enabled)
//#pragma config IESO = ON        // Internal External Switchover bit (Internal/External Switchover mode is enabled)
//#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enabled bit (Fail-Safe Clock Monitor is enabled)
//#pragma config LVP = OFF        // Low Voltage Programming Enable bit (RB3 pin has digital I/O, HV on MCLR must be used for programming)
#FUSES HS
#FUSES NOWDT
#FUSES NOPROTECT
#FUSES NOIESO
#FUSES NOFCMEN


// CONFIG2
//#pragma config BOR4V = BOR40V   // Brown-out Reset Selection bit (Brown-out Reset set to 4.0V)
//#pragma config WRT = OFF        // Flash Program Memory Self Write Enable bits (Write protection off)


#define HXCLK0  PIN_A3
#define HXDAT0  PIN_A2
#define HXCLK1  PIN_A1
#define HXDAT1  PIN_A0
#define HXCLK2  PIN_B1
#define HXDAT2  PIN_B0
#define HXCLK3  PIN_B3
#define HXDAT3  PIN_B2
#define HXCLK4  PIN_B5
#define HXDAT4  PIN_B4

#ZERO_RAM

unsigned int32 Init(unsigned int8 select);
unsigned int32 measure(unsigned int8 select);
char timed_getc();

void main(){
    unsigned int32 mied=0,offset[5]={0};
    unsigned int8 i=0;
    unsigned int8 select=0;
    char recvData='r';
    
    while(TRUE){
        recvData=timed_getc();
        if(recvData=='r'){
            i=0;
            while(i<5){
                offset[i]=Init(i);
                ++i;
            }
            printf("!");
            recvData='a';
        }
        else if(recvData=='q'){
            while(TRUE){
                i=0;
                mied=0;
                while(i<10){
                    mied+=measure(select);
                    ++i;
                }        
                mied/=10;        
                mied-=offset[select];

                while(mied>0xFFFF){
                    ++offset[select];
                    ++mied;
                }

                printf("#%d#%lu#",select,mied);

                if(select<4)    select++;
                else{
                    select=0;
                    printf("!");
                    break;
                }
            }
        }
    } 
}

unsigned int32 Init(unsigned int8 select){
    unsigned int32 offset=0;
    unsigned int8 i=0;
    
    printf("#Init%d#",select);
    while(i<10){
        offset+=measure(select);
        ++i;
    }        
    offset/=10;
    return offset;
}

unsigned int32 measure(unsigned int8 select){
    unsigned int32 Count=0;
    unsigned int8 i=0,A_1=0,A_2=0,A_3=0;
    
    switch(select){
        case 0:
            output_bit(HXDAT0,1);
            output_bit(HXCLK0,0);

            while(input(HXDAT0));

            for(i=0;i<24;i++){
                output_bit(HXCLK0,1);
                Count=Count<<1;
                output_bit(HXCLK0,0);
                if(input(HXDAT0)) ++Count;
            }
            output_bit(HXCLK0,1);
            Count=Count^0x800000;
            output_bit(HXCLK0,0);
            break;
        case 1:
            output_bit(HXDAT1,1);
            output_bit(HXCLK1,0);

            while(input(HXDAT1));

            for(i=0;i<24;i++){
                output_bit(HXCLK1,1);
                Count=Count<<1;
                output_bit(HXCLK1,0);
                if(input(HXDAT1)) ++Count;
            }
            output_bit(HXCLK1,1);
            Count=Count^0x800000;
            output_bit(HXCLK1,0);
            break;
        case 2:
            output_bit(HXDAT2,1);
            output_bit(HXCLK2,0);

            while(input(HXDAT2));

            for(i=0;i<24;i++){
                output_bit(HXCLK2,1);
                Count=Count<<1;
                output_bit(HXCLK2,0);
                if(input(HXDAT2)) ++Count;
            }
            output_bit(HXCLK2,1);
            Count=Count^0x800000;
            output_bit(HXCLK2,0);
            break;
        case 3:
            output_bit(HXDAT3,1);
            output_bit(HXCLK3,0);

            while(input(HXDAT3));

            for(i=0;i<24;i++){
                output_bit(HXCLK3,1);
                Count=Count<<1;
                output_bit(HXCLK3,0);
                if(input(HXDAT3)) ++Count;
            }
            output_bit(HXCLK3,1);
            Count=Count^0x800000;
            output_bit(HXCLK3,0);
            break;
        case 4:
            output_bit(HXDAT4,1);
            output_bit(HXCLK4,0);

            while(input(HXDAT4));

            for(i=0;i<24;i++){
                output_bit(HXCLK4,1);
                Count=Count<<1;
                output_bit(HXCLK4,0);
                if(input(HXDAT4)) ++Count;
            }
            output_bit(HXCLK4,1);
            Count=Count^0x800000;
            output_bit(HXCLK4,0);
            break;
        default:
            break;
    }
    
    A_1=make8(Count,0);
    A_2=make8(Count,1);
    A_3=make8(Count,2);
    A_2=(A_2&0b11111000);
    Count=make16(A_3,A_2);
    
    return Count;
}

char timed_getc() {
   if(kbhit())  return(getc()); 
   else {
      return(0); 
   } 
}














