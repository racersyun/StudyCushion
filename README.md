# Study Tracker Project

## It helps students to study.

http://racersyun.dlinkddns.com/archives/24

### Features

- 사용자가 자리에 앉아있는지 판단하고자 무게를 측정
- 총 다섯 개의 센서를 통해 앉아있는 자세 측정
- USB Micro-5Pin Power Supply
- 측정된 데이터는 블루투스 모듈을 통해 '스터디 트래커' 앱으로 전송

### Device Image



### It runs with..

- PIC16F883 MCU
- HC-06 Bluetooth Module
- HX711 Amplifier Module
- Study Tracker Android Application : https://gitlab.com/racersyun/StudyTracker
